# README #

### What is this repository for? ###

* Ease handling of datasets

### How do I get set up? ###


```
#!R

# install.packages("devtools")
devtools::install_git("https://bitbucket.org/cmbce/r-package-datamanagement/",subdir="DataManagement")
```


### Contribution guidelines ###

Contributions are welcome via clone / pull request.